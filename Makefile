all: sum

sum: main.o sum.o
	g++ -o sum main.o sum.o

main.o: main.cpp sum.h
	g++ -c main.cpp

sum.o: sum.h sum.cpp
	g++ -c sum.cpp

clean:
	rm -f *.o sum

