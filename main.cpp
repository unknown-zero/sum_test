#include <iostream>

#include "sum.h"

int main()
{
	int number = 0;
	std::cin >> number;
	
	int result = sum(number);
	std::cout << "sum = " << result << std::endl;
}
