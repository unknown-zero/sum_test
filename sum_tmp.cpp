#include <iostream>

template <int n>
struct SumofNumber {
	static constexpr int value = n * (n + 1) / 2;
};

int main()
{
	SumofNumber<100> a = SumofNumber<100>();
	std::cout << a.value;
	return 0;
}